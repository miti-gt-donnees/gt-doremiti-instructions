Jeu d'instruction pour fine-tuner un LLM suivant les préconisations du projet Stanford-Alpaca (https://github.com/tatsu-lab/stanford_alpaca)

Ces instructions sont extraites de la FAQ crée par le GT DOREMITI et disponible à cette adresse (https://gt-atelier-donnees.miti.cnrs.fr/faq.html)

Les données sont mise à disposition selon les termes de la Licence Creative Commons Attribution 4.0 International.
